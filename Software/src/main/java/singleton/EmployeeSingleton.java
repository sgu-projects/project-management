package singleton;

import entity.Employee;

public final class EmployeeSingleton {

    private Employee employee;
    private final static EmployeeSingleton INSTANCE = new EmployeeSingleton();

    private EmployeeSingleton() {

    }

    public static EmployeeSingleton getInstance() { return INSTANCE; }

    public void setEmployee(Employee employee) { this.employee = employee; }

    public Employee getEmployee() { return this.employee; }
}
