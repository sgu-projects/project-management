package singleton;

import entity.WorkShift;

public final class WorkShiftSingleton {

    private WorkShift workShift;
    private final static WorkShiftSingleton INSTANCE = new WorkShiftSingleton();

    private WorkShiftSingleton() {}

    public static WorkShiftSingleton getInstance() { return INSTANCE; }
    public void setWorkShift(WorkShift w) { this.workShift = w; }
    public WorkShift getWorkShift() { return this.workShift; }
}
