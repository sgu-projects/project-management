package singleton;

import entity.WorkTable;

public final class WorkTableSingleton {

    private WorkTable workTable;
    private final static WorkTableSingleton INSTANCE = new WorkTableSingleton();

    private WorkTableSingleton() {}

    public static WorkTableSingleton getInstance() { return INSTANCE; }
    public void setWorkTable(WorkTable w) { this.workTable = w; }
    public WorkTable getWorkTable() { return this.workTable; }
}
