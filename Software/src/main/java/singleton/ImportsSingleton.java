package singleton;

import entity.Imports;

public final class ImportsSingleton {

    private Imports imports;
    private final static ImportsSingleton INSTANCE = new ImportsSingleton();

    private ImportsSingleton() {}

    public static ImportsSingleton getInstance() { return INSTANCE; }

    public void setImports(Imports i) { this.imports = i; }

    public Imports getImports() { return this.imports; }
}
