package singleton;

import entity.Customer;

public final class CustomerSingleton {

    private Customer customer;
    private final static CustomerSingleton INSTANCE = new CustomerSingleton();

    private CustomerSingleton() {
    }

    public static CustomerSingleton getInstance() {
        return INSTANCE;
    }

    public void setCustomer(Customer u) {
        this.customer = u;
    }

    public Customer getCustomer() {
        return this.customer;
    }
}
