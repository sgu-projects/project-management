package singleton;

import entity.Merchandise;

public final class MerchandiseSingleton {

    private Merchandise merchandise;
    private final static MerchandiseSingleton INSTANCE = new MerchandiseSingleton();

    private MerchandiseSingleton() {
    }

    public static MerchandiseSingleton getInstance() { return INSTANCE; }

    public void setMerchandise(Merchandise merchandise) { this.merchandise = merchandise; }

    public Merchandise getMerchandise() { return this.merchandise; }
}
