package singleton;

import entity.Orders;

public final class OrdersSingleton {

    private Orders orders;
    private final static OrdersSingleton INSTANCE = new OrdersSingleton();

    private OrdersSingleton() {}

    public static OrdersSingleton getInstance() { return INSTANCE; }

    public void setOrders(Orders orders) { this.orders = orders; }

    public Orders getOrders() { return this.orders; }
}
