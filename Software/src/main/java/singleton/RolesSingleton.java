package singleton;

import entity.Roles;

public final class RolesSingleton {

    private Roles roles;
    private final static RolesSingleton INSTANCE = new RolesSingleton();

    private RolesSingleton() {
    }

    public static RolesSingleton getInstance() { return INSTANCE; }

    public void setRoles(Roles roles) { this.roles = roles; }

    public Roles getRoles() { return this.roles; }
}
