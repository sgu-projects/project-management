package singleton;

import dto.ExportDto;

public final class ExportsSingleton {

    private ExportDto exportDto;
    private final static ExportsSingleton INSTANCE = new ExportsSingleton();

    private ExportsSingleton() {
    }

    public static ExportsSingleton getInstance() { return INSTANCE; }

    public void setReceiptModel(ExportDto u) { this.exportDto = u; }

    public ExportDto getReceiptModel() { return this.exportDto; }
}
