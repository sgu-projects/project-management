package validation;

import entity.Imports;
import entity.Orders;
import entity.Exports;
import repository.ImportsRepository;
import repository.ExportsRepository;

import java.util.ArrayList;
import java.util.List;

public class OrdersValidation {

    public static List<String> validateUpdate(Orders orders) {
        List<String> msg = new ArrayList<>();

        if (orders.getStatus().equals("Hoàn tất")) {
            msg.add("Không được cập nhật đơn hàng đã hoàn tất");
        }

        return msg;
    }

    public static List<String> validateDelete(Orders orders) {
        List<String> msg = new ArrayList<>();

        List<Exports> exportsList = ExportsRepository.getAll();
        List<Imports> importsList = ImportsRepository.getAll();

        if (orders.getStatus().equals("Hoàn tất")) {
            msg.add("Không được xoá đơn hàng đã hoàn tất");
        }
        if (exportsList != null) {
            if (exportsList.stream().anyMatch(t -> t.getOrders().getId().equals(orders.getId()))) {
                msg.add("Không được xoá đơn hàng có xuất hàng");
            }
        }
        if (importsList != null) {
            if (importsList.stream().anyMatch(t -> t.getOrders().getId().equals(orders.getId()))) {
                msg.add("Không được xoá đơn hàng có phiếu nhập hàng");
            }
        }

        return msg;
    }
}
