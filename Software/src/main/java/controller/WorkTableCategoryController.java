package controller;

import dto.WorkTableDto;
import entity.WorkTable;
import singleton.WorkTableSingleton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import repository.WorkTableRepository;
import util.StageHelper;
import util.TableHelper;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;

public class WorkTableCategoryController implements Initializable {
    @FXML
    AnchorPane host;
    @FXML
    private TextField searchBar;

    @FXML
    private TableView<WorkTableDto> contentTable;
    @FXML
    private TableColumn<WorkTableDto, String> nameCol;
    @FXML
    private TableColumn<WorkTableDto, String> shiftCol;
    @FXML
    private TableColumn<WorkTableDto, String> timeInCol;
    @FXML
    private TableColumn<WorkTableDto, String> timeOutCol;
    @FXML
    private TableColumn<WorkTableDto, String> workDaysCol;

    // For other class to call function from this class
    public static WorkTableCategoryController instance;

    public WorkTableCategoryController() {
        instance = this;
    }

    public static WorkTableCategoryController getInstance() {
        return instance;
    }
    ///

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        List<WorkTable> allWorkTable = WorkTableRepository.getAll();
        List<WorkTableDto> allWorkTableDto = new ArrayList<>();
        for (WorkTable item : allWorkTable) {
            WorkTableDto model = new WorkTableDto(item);
            allWorkTableDto.add(model);
        }
        TableHelper.setWorkTableModelTable(allWorkTableDto, contentTable, nameCol, shiftCol, timeInCol, timeOutCol, workDaysCol);
    }

    @FXML
    void openShift(ActionEvent event) {
        MainNavigatorController.getInstance().openShiftTable();
    }

    @FXML
    void save(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("fxml/WorkTableAdd.fxml")));
        StageHelper.startStage(root);
        // Hide host
        MainNavigatorController.instance.getHost().setDisable(true);
    }

    @FXML
    void search(ActionEvent event) {
        List<WorkTable> filteredWorkTable = WorkTableRepository.getByEmployeeOrShift(searchBar.getText());
        List<WorkTableDto> filteredWorkTableDto = new ArrayList<>();
        for (WorkTable item : filteredWorkTable) {
            WorkTableDto model = new WorkTableDto(item);
            filteredWorkTableDto.add(model);
        }
        TableHelper.setWorkTableModelTable(filteredWorkTableDto, contentTable, nameCol, shiftCol, timeInCol, timeOutCol, workDaysCol);
    }

    @FXML
    void select(MouseEvent event) throws IOException {
        if (event.getClickCount() == 2) {
            WorkTable chosenWorkTable = contentTable.getSelectionModel().getSelectedItem().getWorkTable();
            contentTable.getSelectionModel().clearSelection();
            // Store WorkTable to use in another class
            if (chosenWorkTable != null) {
                WorkTableSingleton.getInstance().setWorkTable(chosenWorkTable);
                Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("fxml/WorkTableUpdate.fxml")));
                StageHelper.startStage(root);
                // Hide host
                MainNavigatorController.instance.getHost().setDisable(true);
            }
        }
    }

    @FXML
    void requestFocus(MouseEvent event) {
        host.requestFocus();
    }
}
