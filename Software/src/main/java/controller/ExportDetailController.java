package controller;

import dto.OrdersDetailDto;
import dto.ExportOrdersDto;
import entity.Exports;
import entity.Orders;
import entity.OrdersDetail;
import singleton.ExportsSingleton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import org.hibernate.Session;
import repository.OrdersDetailRepository;
import util.*;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

public class ExportDetailController implements Initializable {
    @FXML
    private TextField customerHolder;
    @FXML
    private TextField phoneHolder;

    // Orders table
    @FXML
    private TableView<ExportOrdersDto> ordersTable;
    @FXML
    private TableColumn<ExportOrdersDto, Date> dateCol;
    @FXML
    private TableColumn<ExportOrdersDto, String> descriptionCol;
    @FXML
    private TableColumn<ExportOrdersDto, String> employeeCol;

    @FXML
    private TextField addressHolder;

    // Detail table
    @FXML
    private TableView<OrdersDetailDto> detailTable;
    @FXML
    private TableColumn<OrdersDetailDto, String> merchandiseCol;
    @FXML
    private TableColumn<OrdersDetailDto, Integer> quantityCol;
    @FXML
    private TableColumn<OrdersDetailDto, Integer> amountCol;
    @FXML
    private TableColumn<OrdersDetailDto, Integer> finalAmountCol;

    @FXML
    private TextField sumQuantityHolder;
    @FXML
    private TextField sumAmountHolder;

    // Get ReceiptModel from ReceiptCategoryController select(MouseEvent event)
    Exports exports = ExportsSingleton.getInstance().getReceiptModel().getReceipt();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        if (exports != null) {
            // Set customer
            customerHolder.setText(exports.getOrders().getCustomer().getFullName());
            phoneHolder.setText(exports.getOrders().getCustomer().getPhone());
            addressHolder.setText(exports.getOrders().getCustomer().getAddress());
            // Set orders
            List<ExportOrdersDto> exportOrdersDtoList = new ArrayList<>();
            ExportOrdersDto exportOrdersDto = new ExportOrdersDto();
            exportOrdersDto.setOrders(exports.getOrders());
            exportOrdersDto.setCreatedDate(exports.getOrders().getCreatedDate());
            exportOrdersDto.setDescription(exports.getOrders().getDescription());
            exportOrdersDto.setEmployeeName(exports.getEmployee().getFullName());
            exportOrdersDtoList.add(exportOrdersDto);
            TableHelper.setExportsOrdersModelTable(exportOrdersDtoList, ordersTable, dateCol, descriptionCol, employeeCol);
            // Set orders detail
            List<OrdersDetail> ordersDetailList = OrdersDetailRepository.getByOrdersId(exports.getOrders().getId());
            List<OrdersDetailDto> ordersDetailDtoList = new ArrayList<>();
            for (OrdersDetail item : ordersDetailList) {
                OrdersDetailDto ordersDetailDto = new OrdersDetailDto();
                ordersDetailDto.setMerchandiseName(item.getMerchandise().getName());
                ordersDetailDto.setQuantity(item.getQuantity());
                ordersDetailDto.setAmount(NumberHelper.addComma(item.getMerchandise().getPrice().toString()));
                ordersDetailDto.setFinalAmount(NumberHelper.addComma(String.valueOf(item.getAmount())));
                ordersDetailDtoList.add(ordersDetailDto);
            }
            TableHelper.setOrdersDetailModelTable(ordersDetailDtoList, detailTable, merchandiseCol, quantityCol, amountCol, finalAmountCol);
            // Set sumQuantity and sumAmount of orders detail
            Integer sumQuantity = ordersDetailList.stream().mapToInt(OrdersDetail::getQuantity).sum();
            Long sumAmount = ordersDetailList.stream().mapToLong(OrdersDetail::getAmount).sum();
            sumQuantityHolder.setText(NumberHelper.addComma(String.valueOf(sumQuantity)));
            sumAmountHolder.setText(NumberHelper.addComma(String.valueOf(sumAmount)));
        }
    }

    @FXML
    void delete(ActionEvent event) {
        // Delete receipt
        Session session = HibernateUtils.getSessionFactory().openSession();
        session.beginTransaction();
        session.delete(exports);
        session.getTransaction().commit();
        session.close();
        // Update orders status
        Orders orders = exports.getOrders();
        orders.setStatus("Chưa hoàn tất");
        session = HibernateUtils.getSessionFactory().openSession();
        session.beginTransaction();
        session.saveOrUpdate(orders);
        session.getTransaction().commit();
        session.close();
        // Refresh content table
        ExportCategoryController.getInstance().refresh();
        OrderCategoryController.getInstance().refresh();
        // Set receipt model holder
        ExportsSingleton.getInstance().setReceiptModel(null);
        // Close stage
        StageHelper.closeStage(event);
        // Show alert box
        AlertBoxHelper.showMessageBox("Xoá thành công");
        // Unhide host
        MainNavigatorController.instance.getHost().setDisable(false);
        // Update Statistic
        if (StatisticRevenueController.getInstance() != null) {
            StatisticRevenueController.getInstance().refresh(null);
        }
        if (StatisticCustomerController.getInstance() != null) {
            StatisticCustomerController.getInstance().refresh(null);
        }
        if (StatisticMerchandiseController.getInstance() != null) {
            StatisticMerchandiseController.instance.refresh(null);
        }
    }

    @FXML
    void close(MouseEvent event) {
        StageHelper.closeStage(event);
        // Set receipt model holder
        ExportsSingleton.getInstance().setReceiptModel(null);
        // Unhide host
        MainNavigatorController.instance.getHost().setDisable(false);
    }
}
