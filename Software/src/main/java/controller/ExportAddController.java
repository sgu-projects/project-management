package controller;

import com.jfoenix.controls.JFXButton;
import dto.OrdersDetailDto;
import dto.ExportOrdersDto;
import entity.Customer;
import entity.Exports;
import entity.Orders;
import entity.OrdersDetail;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import org.controlsfx.control.textfield.AutoCompletionBinding;
import org.controlsfx.control.textfield.TextFields;
import org.hibernate.Session;
import repository.CustomerRepository;
import repository.OrdersDetailRepository;
import repository.OrdersRepository;
import util.*;

import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

public class ExportAddController implements Initializable {
    @FXML
    private AnchorPane host;
    @FXML
    private TextField customerHolder;
    @FXML
    private TextField phoneHolder;

    // Orders table
    @FXML
    private TableView<ExportOrdersDto> ordersTable;
    @FXML
    private TableColumn<ExportOrdersDto, Date> dateCol;
    @FXML
    private TableColumn<ExportOrdersDto, String> descriptionCol;
    @FXML
    private TableColumn<ExportOrdersDto, String> employeeCol;

    @FXML
    private TextField addressHolder;

    // Detail table
    @FXML
    private TableView<OrdersDetailDto> detailTable;
    @FXML
    private TableColumn<OrdersDetailDto, String> merchandiseCol;
    @FXML
    private TableColumn<OrdersDetailDto, Integer> quantityCol;
    @FXML
    private TableColumn<OrdersDetailDto, Integer> amountCol;
    @FXML
    private TableColumn<OrdersDetailDto, Integer> finalAmountCol;

    @FXML
    private JFXButton saveButton;
    @FXML
    private TextField sumQuantityHolder;
    @FXML
    private TextField sumAmountHolder;

    private Orders chosenOrders;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        // Add customer to choose customer textfield
        List<Customer> customerList = CustomerRepository.getAllCustomerActiveOrders();
        if (customerList != null && customerList.size() > 0) {
            // Add item to Customer textfield
            AutoCompletionBinding<String> cHolder = TextFields.bindAutoCompletion(customerHolder, customerList.stream().map(Customer::getFullName).collect(Collectors.toList()));
            cHolder.setOnAutoCompleted(stringAutoCompletionEvent -> showChosenCustomer(null));
        }
    }

    @FXML
    void showChosenCustomer(ActionEvent event) {
        // Clear detail table, sumQuantityHolder, sumAmountHolder
        detailTable.getItems().clear();
        sumAmountHolder.clear();
        sumQuantityHolder.clear();

        // Show customer info
        Customer customer = CustomerRepository.getByName(customerHolder.getText());
        phoneHolder.setText(customer.getPhone());
        addressHolder.setText(customer.getAddress());
        // Show customer orders info
        List<Orders> ordersList = OrdersRepository.getActiveByCustomerName(customerHolder.getText());
        if (ordersList != null && ordersList.size() > 0) {
            List<ExportOrdersDto> exportOrdersDtoList = new ArrayList<>();
            for (Orders item : ordersList) {
                ExportOrdersDto exportOrdersDto = new ExportOrdersDto();
                exportOrdersDto.setOrders(item);
                exportOrdersDto.setCreatedDate(item.getCreatedDate());
                exportOrdersDto.setDescription(item.getDescription());
                exportOrdersDto.setEmployeeName(item.getEmployee().getFullName());
                exportOrdersDtoList.add(exportOrdersDto);
            }
            TableHelper.setExportsOrdersModelTable(exportOrdersDtoList, ordersTable, dateCol, descriptionCol, employeeCol);
        }
    }

    @FXML
    void select(MouseEvent event) {
        if (event.getClickCount() == 2) {
            // Show chosen orders detail in detailTable
            Orders orders = new Orders(ordersTable.getSelectionModel().getSelectedItem().getOrders());
            ordersTable.getSelectionModel().clearSelection();
            List<OrdersDetail> ordersDetailList = OrdersDetailRepository.getByOrdersId(orders.getId());
            if (ordersDetailList != null && !ordersDetailList.isEmpty()) {
                List<OrdersDetailDto> ordersDetailDtoList = new ArrayList<>();
                for (OrdersDetail item : ordersDetailList) {
                    OrdersDetailDto ordersDetailDto = new OrdersDetailDto();
                    ordersDetailDto.setMerchandiseName(item.getMerchandise().getName());
                    ordersDetailDto.setQuantity(item.getQuantity());
                    ordersDetailDto.setAmount(NumberHelper.addComma(item.getMerchandise().getPrice().toString()));
                    ordersDetailDto.setFinalAmount(NumberHelper.addComma(String.valueOf(item.getAmount())));
                    ordersDetailDtoList.add(ordersDetailDto);
                }
                // set SumQuantityHolder and SumAmountHolder
                Integer sumQuantity = ordersDetailDtoList.stream().mapToInt(OrdersDetailDto::getQuantity).sum();
                Integer sumAmount = ordersDetailDtoList.stream().mapToInt(t -> Integer.parseInt(NumberHelper.removeComma(t.getFinalAmount()))).sum();
                sumQuantityHolder.setText(String.valueOf(sumQuantity));
                sumAmountHolder.setText(NumberHelper.addComma(String.valueOf(sumAmount)));
                // Set table
                TableHelper.setOrdersDetailModelTable(ordersDetailDtoList, detailTable, merchandiseCol, quantityCol, amountCol, finalAmountCol);
                saveButton.setDisable(false);
                // Set chosen orders
                chosenOrders = orders;
            }
        }
    }

    @FXML
    void save(ActionEvent event) {
        Session session;
        // Save new receipt
        Exports exports = new Exports();
        exports.setId(UUIDHelper.generateType4UUID().toString());
        exports.setOrders(chosenOrders);
        exports.setEmployee(chosenOrders.getEmployee());
        exports.setDescription(chosenOrders.getDescription());

        session = HibernateUtils.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(exports);
        session.getTransaction().commit();
        session.close();

        // Close stage
        StageHelper.closeStage(event);
        // Show alert box
        AlertBoxHelper.showMessageBox("Thêm thành công");
        // Refresh content table
        ExportCategoryController.getInstance().refresh();
        // Unhide host
        MainNavigatorController.instance.getHost().setDisable(false);

        // Update orders status
        chosenOrders.setStatus("Hoàn tất");
        session = HibernateUtils.getSessionFactory().openSession();
        session.beginTransaction();
        session.saveOrUpdate(chosenOrders);
        session.getTransaction().commit();
        session.close();
        OrderCategoryController.getInstance().initialize(null, null);
        // Update Statistic
        if (StatisticRevenueController.getInstance() != null) {
            StatisticRevenueController.getInstance().refresh(null);
        }
        if (StatisticCustomerController.getInstance() != null) {
            StatisticCustomerController.getInstance().refresh(null);
        }
        if (StatisticMerchandiseController.getInstance() != null) {
            StatisticMerchandiseController.instance.refresh(null);
        }
    }

    @FXML
    void close(MouseEvent event) {
        StageHelper.closeStage(event);
        // Unhide host
        MainNavigatorController.instance.getHost().setDisable(false);
    }

    @FXML
    void requestFocus(MouseEvent event) {
        host.requestFocus();
    }
}
