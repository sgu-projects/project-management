package controller;

import dto.OrdersDto;
import dto.search.OrderSearchDto;
import entity.Orders;
import entity.OrdersDetail;
import singleton.OrdersSingleton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import repository.OrdersDetailRepository;
import repository.OrdersRepository;
import util.NumberHelper;
import util.StageHelper;
import util.TableHelper;

import java.io.IOException;
import java.net.URL;
import java.util.*;

public class OrderCategoryController implements Initializable {
    @FXML
    private AnchorPane host;
    @FXML
    private TextField searchBar;

    @FXML
    private TableView<OrdersDto> contentTable;
    @FXML
    private TableColumn<OrdersDto, Date> createdDateCol;
    @FXML
    private TableColumn<OrdersDto, String> customerNameCol;
    @FXML
    private TableColumn<OrdersDto, String> descriptionCol;
    @FXML
    private TableColumn<OrdersDto, String> totalAmountCol;
    @FXML
    private TableColumn<OrdersDto, String> statusCol;
    @FXML
    private TableColumn<OrdersDto, String> typeCol;

    public Boolean ordersAddUpdateIsShow = false;

    public static String searchCustomerName;
    public static Date searchFromDate;
    public static Date searchToDate;
    public static String searchType;
    public static String searchStatus;

    // For other class cal function from this class
    public static OrderCategoryController instance;

    public OrderCategoryController() {
        instance = this;
    }

    public static OrderCategoryController getInstance() {
        return instance;
    }
    ///

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        List<Orders> ordersList = OrdersRepository.getAll();
        List<OrdersDto> ordersDtoList = new ArrayList<>();
        for (Orders item : ordersList) {
            List<OrdersDetail> ordersDetailList = OrdersDetailRepository.getByOrdersId(item.getId());

            OrdersDto ordersDto = new OrdersDto();
            ordersDto.setCreatedDate(item.getCreatedDate());
            ordersDto.setCustomerName(item.getCustomer().getFullName());
            ordersDto.setDescription(item.getDescription());
            ordersDto.setSumAmount(NumberHelper.addComma(String.valueOf(ordersDetailList.stream().mapToLong(OrdersDetail::getAmount).sum())));
            ordersDto.setStatus(item.getStatus());
            ordersDto.setType(item.getType());
            ordersDto.setOrders(item);
            ordersDtoList.add(ordersDto);
        }
        // Populate table
        TableHelper.setOrdersModelTable(ordersDtoList, contentTable, createdDateCol, customerNameCol, descriptionCol, totalAmountCol, statusCol, typeCol);
    }

    @FXML
    void openAdvanceSearch(MouseEvent event) {
        try {
            Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("fxml/OrderAdvanceSearch.fxml")));
            StageHelper.startStage(root);
            // HIDE HOST
            MainNavigatorController.instance.getHost().setDisable(true);
        } catch (IOException e) {
            System.out.println(e.getMessage());
            System.out.println(Arrays.toString(e.getStackTrace()));
        }
    }

    @FXML
    void addOrder(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("fxml/OrderAdd.fxml")));
        StageHelper.startStage(root);
        //Hide host
        MainNavigatorController.instance.getHost().setDisable(true);
    }

    @FXML
    void select(MouseEvent event) throws IOException {
        if (event.getClickCount() == 2) {
            Orders orders = contentTable.getSelectionModel().getSelectedItem().getOrders();
            contentTable.getSelectionModel().clearSelection();
            // Store Orders to use in another class
            if (orders != null) {
                OrdersSingleton.getInstance().setOrders(orders);
                Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("fxml/OrderUpdate.fxml")));
                StageHelper.startStage(root);
                // Hide host
                MainNavigatorController.instance.getHost().setDisable(true);
            }
        }
    }

    @FXML
    void refresh(ActionEvent event) {
        searchCustomerName = null;
        searchFromDate = null;
        searchToDate = null;
        searchType = null;
        searchStatus = null;

        this.initialize(null, null);
    }

    public void applyFilter(List<Orders> filterResult, OrderSearchDto searchModel) {
        if (filterResult != null) {
            searchCustomerName = searchModel.getCustomerName();
            searchFromDate = searchModel.getFromDate();
            searchToDate = searchModel.getToDate();
            searchType = searchModel.getOrderType();
            searchStatus = searchModel.getOrderStatus();

            List<OrdersDto> ordersDtoList = new ArrayList<>();
            for (Orders item : filterResult) {
                List<OrdersDetail> ordersDetailList = OrdersDetailRepository.getByOrdersId(item.getId());

                OrdersDto ordersDto = new OrdersDto();
                ordersDto.setCreatedDate(item.getCreatedDate());
                ordersDto.setCustomerName(item.getCustomer().getFullName());
                ordersDto.setDescription(item.getDescription());
                ordersDto.setSumAmount(NumberHelper.addComma(String.valueOf(ordersDetailList.stream().mapToLong(OrdersDetail::getAmount).sum())));
                ordersDto.setStatus(item.getStatus());
                ordersDto.setType(item.getType());
                ordersDto.setOrders(item);
                ordersDtoList.add(ordersDto);
            }
            // Populate table
            TableHelper.setOrdersModelTable(ordersDtoList, contentTable, createdDateCol, customerNameCol, descriptionCol, totalAmountCol, statusCol, typeCol);
        }
    }

    @FXML
    void requestFocus(MouseEvent event) {
        host.requestFocus();
    }

    void refresh() {
        this.initialize(null, null);
    }
}
