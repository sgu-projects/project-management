package controller;

import dto.ExportDto;
import entity.Exports;
import singleton.ExportsSingleton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import repository.OrdersDetailRepository;
import repository.ExportsRepository;
import util.NumberHelper;
import util.StageHelper;
import util.TableHelper;

import java.io.IOException;
import java.net.URL;
import java.util.*;

public class ExportCategoryController implements Initializable {
    @FXML
    private AnchorPane host;
    @FXML
    private TableView<ExportDto> contentTable;
    @FXML
    private TableColumn<ExportDto, Date> dateCol;
    @FXML
    private TableColumn<ExportDto, String> nameCol;
    @FXML
    private TableColumn<ExportDto, String> descriptionCol;
    @FXML
    private TableColumn<ExportDto, Integer> quantityCol;
    @FXML
    private TableColumn<ExportDto, String> amountCol;
    @FXML
    private TextField searchBar;

    // For other class call function from this class
    public static ExportCategoryController instance;

    public ExportCategoryController() {
        instance = this;
    }

    public static ExportCategoryController getInstance() {
        return instance;
    }
    ///

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        // Get all receipt
        List<Exports> exportsList = ExportsRepository.getAll();
        //Set receipt model
        if (exportsList != null && !exportsList.isEmpty()) {
            List<ExportDto> exportDtoList = new ArrayList<>();
            for (Exports item : exportsList) {
                Long sumAmount = OrdersDetailRepository.getSumAmountById(item.getOrders().getId());
                int sumQuantity = Math.toIntExact(OrdersDetailRepository.getSumQuantityById(item.getOrders().getId()));

                ExportDto exportDto = new ExportDto();
                exportDto.setReceipt(item);
                exportDto.setCreatedDate(item.getCreatedDate());
                exportDto.setCustomerName(item.getOrders().getCustomer().getFullName());
                exportDto.setDescription(item.getDescription());
                exportDto.setSumQuantity(sumQuantity);
                exportDto.setSumAmount(NumberHelper.addComma(String.valueOf(sumAmount)));
                exportDtoList.add(exportDto);
            }
            TableHelper.setExportsTable(exportDtoList, contentTable, dateCol, nameCol, descriptionCol, quantityCol, amountCol);
        }
    }

    @FXML
    void addReceipt(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("fxml/ExportsAdd.fxml")));
        StageHelper.startStage(root);
        // Hide host
        MainNavigatorController.instance.getHost().setDisable(true);
    }

    @FXML
    void search(ActionEvent event) {
        // Get receipt like name
        List<Exports> exportsList = ExportsRepository.getLikeCustomerName(searchBar.getText());
        //Set receipt model
        if (exportsList != null && !exportsList.isEmpty()) {
            List<ExportDto> exportDtoList = new ArrayList<>();
            for (Exports item : exportsList) {
                Long sumAmount = OrdersDetailRepository.getSumAmountById(item.getOrders().getId());
                Integer sumQuantity = Math.toIntExact(OrdersDetailRepository.getSumQuantityById(item.getOrders().getId()));

                ExportDto exportDto = new ExportDto();
                exportDto.setReceipt(item);
                exportDto.setCreatedDate(item.getCreatedDate());
                exportDto.setCustomerName(item.getOrders().getCustomer().getFullName());
                exportDto.setDescription(item.getDescription());
                exportDto.setSumQuantity(sumQuantity);
                exportDto.setSumAmount(NumberHelper.addComma(String.valueOf(sumAmount)));
                exportDtoList.add(exportDto);
            }
            TableHelper.setExportsTable(exportDtoList, contentTable, dateCol, nameCol, descriptionCol, quantityCol, amountCol);
        }
    }

    @FXML
    void select(MouseEvent event) throws IOException {
        if (event.getClickCount() == 2) {
            ExportDto exportDto = contentTable.getSelectionModel().getSelectedItem();
            contentTable.getSelectionModel().clearSelection();
            // Store receipt to use in another class
            if (exportDto != null) {
                ExportsSingleton.getInstance().setReceiptModel(exportDto);
                Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("fxml/ExportsDetail.fxml")));
                StageHelper.startStage(root);
                // Hide host
                MainNavigatorController.instance.getHost().setDisable(true);
            }
        }
    }

    // Refresh table
    public void refresh() {
        initialize(null, null);
    }

    @FXML
    void requestFocus(MouseEvent event) {
        host.requestFocus();
    }
}
