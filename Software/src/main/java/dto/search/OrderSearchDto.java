package dto.search;

import lombok.Data;

import java.util.Date;

@Data
public class OrderSearchDto {
    private String customerName;
    private String orderStatus;
    private String orderType;
    private Date fromDate;
    private Date toDate;
}
