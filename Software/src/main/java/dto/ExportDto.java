package dto;

import entity.Exports;

import java.util.Date;

public class ExportDto {
    private Exports exports;
    private Date createdDate;
    private String customerName;
    private String description;
    private Integer sumQuantity;
    private String sumAmount;

    public ExportDto() {
    }

    public ExportDto(Exports exports, Date createdDate, String customerName, String description, Integer sumQuantity, String sumAmount) {
        this.exports = exports;
        this.createdDate = createdDate;
        this.customerName = customerName;
        this.description = description;
        this.sumQuantity = sumQuantity;
        this.sumAmount = sumAmount;
    }

    public Exports getReceipt() {
        return exports;
    }

    public void setReceipt(Exports exports) {
        this.exports = exports;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getSumQuantity() {
        return sumQuantity;
    }

    public void setSumQuantity(Integer sumQuantity) {
        this.sumQuantity = sumQuantity;
    }

    public String getSumAmount() {
        return sumAmount;
    }

    public void setSumAmount(String sumAmount) {
        this.sumAmount = sumAmount;
    }
}
