package util;

import dto.*;
import entity.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class TableHelper {

    public static void setCustomerTable(List<Customer> customerList,
                                        TableView<Customer> table,
                                        TableColumn<Customer, String> nameCol,
                                        TableColumn<Customer, String> phoneCol,
                                        TableColumn<Customer, String> emailCol,
                                        TableColumn<Customer, String> addressCol,
                                        TableColumn<Customer, String> typeCol) {
        table.getItems().clear();
        ObservableList<Customer> data = FXCollections.observableList(customerList);

        // Associate data with columns
        nameCol.setCellValueFactory(new PropertyValueFactory<>("fullName"));
        phoneCol.setCellValueFactory(new PropertyValueFactory<>("phone"));
        emailCol.setCellValueFactory(new PropertyValueFactory<>("email"));
        addressCol.setCellValueFactory(new PropertyValueFactory<>("address"));
        typeCol.setCellValueFactory(new PropertyValueFactory<>("type"));

        // Add item table
        table.getItems().clear();
        table.setItems(data);
    }

    public static void setEmployeeTable(List<Employee> employeeList,
                                        TableView<Employee> table,
                                        TableColumn<Employee, String> nameCol,
                                        TableColumn<Employee, String> phoneCol,
                                        TableColumn<Employee, String> emailCol,
                                        TableColumn<Employee, Date> dateOfBirthCol) {
        table.getItems().clear();
        ObservableList<Employee> data = FXCollections.observableList(employeeList);

        // Associate data with columns
        nameCol.setCellValueFactory(new PropertyValueFactory<>("fullName"));
        phoneCol.setCellValueFactory(new PropertyValueFactory<>("phone"));
        emailCol.setCellValueFactory(new PropertyValueFactory<>("email"));
        dateOfBirthCol.setCellValueFactory(new PropertyValueFactory<>("birthDay"));

        // Add item table
        table.getItems().clear();
        table.setItems(data);
    }

    public static void setPermissionNameTable(List<Permissions> permissionsList,
                                              TableView<Permissions> table,
                                              TableColumn<Permissions, String> nameCol) {
        table.getItems().clear();
        ObservableList<Permissions> data = FXCollections.observableList(permissionsList);

        // Associate data with columns
        nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));

        // Add item table
        table.getItems().clear();
        table.setItems(data);
    }

    public static void setRolesTable(List<Roles> rolesList,
                                     TableView<Roles> table,
                                     TableColumn<Roles, String> nameCol,
                                     TableColumn<Roles, Date> createdDateCol) {
        table.getItems().clear();
        ObservableList<Roles> data = FXCollections.observableList(rolesList);

        // Associate data with columns
        nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        createdDateCol.setCellValueFactory(new PropertyValueFactory<>("createdDate"));

        // Add item table
        table.getItems().clear();
        table.setItems(data);
    }

    public static void setExportsTable(List<ExportDto> receiptModeltList,
                                       TableView<ExportDto> table,
                                       TableColumn<ExportDto, Date> dateCol,
                                       TableColumn<ExportDto, String> nameCol,
                                       TableColumn<ExportDto, String> descriptionCol,
                                       TableColumn<ExportDto, Integer> quantityCol,
                                       TableColumn<ExportDto, String> amountCol) {
        table.getItems().clear();
        ObservableList<ExportDto> data = FXCollections.observableList(receiptModeltList);

        // Associate data with columns
        dateCol.setCellValueFactory(new PropertyValueFactory<>("createdDate"));
        nameCol.setCellValueFactory(new PropertyValueFactory<>("customerName"));
        descriptionCol.setCellValueFactory(new PropertyValueFactory<>("description"));
        quantityCol.setCellValueFactory(new PropertyValueFactory<>("sumQuantity"));
        amountCol.setCellValueFactory(new PropertyValueFactory<>("sumAmount"));

        // Add item table
        table.getItems().clear();
        table.setItems(data);
    }

    public static void setOrdersModelTable(List<OrdersDto> ordersDtoList,
                                           TableView<OrdersDto> table,
                                           TableColumn<OrdersDto, Date> createdDateCol,
                                           TableColumn<OrdersDto, String> customerNameCol,
                                           TableColumn<OrdersDto, String> descriptionCol,
                                           TableColumn<OrdersDto, String> totalAmountCol,
                                           TableColumn<OrdersDto, String> statusCol,
                                           TableColumn<OrdersDto, String> typeCol) {
        table.getItems().clear();
        ObservableList<OrdersDto> data = FXCollections.observableList(ordersDtoList);

        // Associate data with columns
        createdDateCol.setCellValueFactory(new PropertyValueFactory<>("createdDate"));
        customerNameCol.setCellValueFactory(new PropertyValueFactory<>("customerName"));
        totalAmountCol.setCellValueFactory(new PropertyValueFactory<>("sumAmount"));
        statusCol.setCellValueFactory(new PropertyValueFactory<>("status"));
        typeCol.setCellValueFactory(new PropertyValueFactory<>("type"));
        descriptionCol.setCellValueFactory(new PropertyValueFactory<>("description"));

        // Add item table
        table.getItems().clear();
        table.setItems(data);
    }

    public static void setMerchandiseTable(List<Merchandise> merchandiseList,
                                           TableView<MerchandiseDto> table,
                                           TableColumn<Merchandise, String> nameCol,
                                           TableColumn<Merchandise, String> typeCol,
                                           TableColumn<Merchandise, Integer> quantityCol,
                                           TableColumn<Merchandise, String> priceCol) {
        try {
            table.getItems().clear();

            List<MerchandiseDto> merchandiseDtos = new ArrayList<>();

            // Add comma for price and import price
            for (Merchandise item : merchandiseList) {
                MerchandiseDto merchandiseDto = item.toMerchandiseModel();

                merchandiseDto.setImportPrice(item.getImportPrice() != null ? NumberHelper.addComma(item.getImportPrice()) : null);
                merchandiseDto.setPrice(item.getPrice() != null ? NumberHelper.addComma(merchandiseDto.getPrice()) : null);

                merchandiseDtos.add(merchandiseDto);
            }
            ObservableList<MerchandiseDto> data = FXCollections.observableList(merchandiseDtos);

            // Associate data with columns
            nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
            typeCol.setCellValueFactory(new PropertyValueFactory<>("type"));
            quantityCol.setCellValueFactory(new PropertyValueFactory<>("quantity"));
            priceCol.setCellValueFactory(new PropertyValueFactory<>("price"));

            // Add item table
            table.getItems().clear();
            table.setItems(data);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println(Arrays.toString(e.getStackTrace()));
        }
    }

    public static void setExportsOrdersModelTable(List<ExportOrdersDto> exportOrdersDtoList,
                                                  TableView<ExportOrdersDto> table,
                                                  TableColumn<ExportOrdersDto, Date> dateCol,
                                                  TableColumn<ExportOrdersDto, String> descriptionCol,
                                                  TableColumn<ExportOrdersDto, String> employeeCol) {
        ObservableList<ExportOrdersDto> data = FXCollections.observableList(exportOrdersDtoList);

        // Associate data with columns
        dateCol.setCellValueFactory(new PropertyValueFactory<>("createdDate"));
        descriptionCol.setCellValueFactory(new PropertyValueFactory<>("description"));
        employeeCol.setCellValueFactory(new PropertyValueFactory<>("employeeName"));

        //Add item table
        table.setItems(data);
    }
    public static void setOrdersAddTable(List<OrdersAddTableDto> ordersAddTableDtoList,
                                         TableView<OrdersAddTableDto> table,
                                         TableColumn<OrdersAddTableDto, String> merchandiseCol,
                                         TableColumn<OrdersAddTableDto, Integer> quantityCol,
                                         TableColumn<OrdersAddTableDto, String> amountCol,
                                         TableColumn<OrdersAddTableDto, String> sumAmountCol) {
        ObservableList<OrdersAddTableDto> data = FXCollections.observableList(ordersAddTableDtoList);

        // Associate data with columns
        merchandiseCol.setCellValueFactory(new PropertyValueFactory<>("merchandiseName"));
        quantityCol.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        amountCol.setCellValueFactory(new PropertyValueFactory<>("amount"));
        sumAmountCol.setCellValueFactory(new PropertyValueFactory<>("sumAmount"));

        // Add item table
        table.setItems(data);
    }

    public static void setOrdersDetailModelTable(List<OrdersDetailDto> ordersDetailDtoList,
                                                 TableView<OrdersDetailDto> table,
                                                 TableColumn<OrdersDetailDto, String> merchandiseCol,
                                                 TableColumn<OrdersDetailDto, Integer> quantityCol,
                                                 TableColumn<OrdersDetailDto, Integer> amountCol,
                                                 TableColumn<OrdersDetailDto, Integer> finalAmountCol) {
        ObservableList<OrdersDetailDto> data = FXCollections.observableList(ordersDetailDtoList);

        // Associate date with columns
        merchandiseCol.setCellValueFactory(new PropertyValueFactory<>("merchandiseName"));
        quantityCol.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        amountCol.setCellValueFactory(new PropertyValueFactory<>("amount"));
        finalAmountCol.setCellValueFactory(new PropertyValueFactory<>("finalAmount"));

        // Add item table
        table.setItems(data);
    }

    public static void setImportsDetailModelTable(List<ImportsDetailDto> ordersDetailModelList,
                                                 TableView<ImportsDetailDto> table,
                                                 TableColumn<ImportsDetailDto, String> merchandiseCol,
                                                 TableColumn<ImportsDetailDto, Integer> quantityCol,
                                                 TableColumn<ImportsDetailDto, Integer> amountCol,
                                                 TableColumn<ImportsDetailDto, Integer> finalAmountCol) {
        ObservableList<ImportsDetailDto> data = FXCollections.observableList(ordersDetailModelList);

        // Associate date with columns
        merchandiseCol.setCellValueFactory(new PropertyValueFactory<>("merchandiseName"));
        quantityCol.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        amountCol.setCellValueFactory(new PropertyValueFactory<>("amount"));
        finalAmountCol.setCellValueFactory(new PropertyValueFactory<>("finalAmount"));

        // Add item table
        table.setItems(data);
    }

    public static void setImportsModelTable(List<ImportsDto> importsDtoList,
                                            TableView<ImportsDto> table,
                                            TableColumn<ImportsDto, Date> dateCol,
                                            TableColumn<ImportsDto, String> nameCol,
                                            TableColumn<ImportsDto, String> descriptionCol,
                                            TableColumn<ImportsDto, Integer> quantityCol,
                                            TableColumn<ImportsDto, String> amountCol) {

        ObservableList<ImportsDto> data = FXCollections.observableList(importsDtoList);

        // Associate data with columns
        dateCol.setCellValueFactory(new PropertyValueFactory<>("createdDate"));
        nameCol.setCellValueFactory(new PropertyValueFactory<>("customerName"));
        descriptionCol.setCellValueFactory(new PropertyValueFactory<>("description"));
        quantityCol.setCellValueFactory(new PropertyValueFactory<>("sumQuantity"));
        amountCol.setCellValueFactory(new PropertyValueFactory<>("sumAmount"));

        // Add item to table
        table.setItems(data);
    }

    public static void setWorkShiftTable(List<WorkShift> workShiftList,
                                         TableView<WorkShift> table,
                                         TableColumn<WorkShift, String> dateCol,
                                         TableColumn<WorkShift, String> nameCol,
                                         TableColumn<WorkShift, String> timeInCol,
                                         TableColumn<WorkShift, String> timeOutCol) {
        ObservableList<WorkShift> data = FXCollections.observableList(workShiftList);

        // Associate data with columns
        dateCol.setCellValueFactory(new PropertyValueFactory<>("createdDate"));
        nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        timeInCol.setCellValueFactory(new PropertyValueFactory<>("startTime"));
        timeOutCol.setCellValueFactory(new PropertyValueFactory<>("endTime"));

        // Add item to table
        table.setItems(data);
    }

    public static void setWorkTableModelTable(List<WorkTableDto> dataList,
                                              TableView<WorkTableDto> table,
                                              TableColumn<WorkTableDto, String> nameCol,
                                              TableColumn<WorkTableDto, String> shiftCol,
                                              TableColumn<WorkTableDto, String> timeInCol,
                                              TableColumn<WorkTableDto, String> timeOutCol,
                                              TableColumn<WorkTableDto, String> workDaysCol) {
        ObservableList<WorkTableDto> data = FXCollections.observableList(dataList);

        // Associate data with columns
        nameCol.setCellValueFactory(new PropertyValueFactory<>("employeeName"));
        shiftCol.setCellValueFactory(new PropertyValueFactory<>("shiftName"));
        timeInCol.setCellValueFactory(new PropertyValueFactory<>("startTime"));
        timeOutCol.setCellValueFactory(new PropertyValueFactory<>("endTime"));
        workDaysCol.setCellValueFactory(new PropertyValueFactory<>("workDays"));

        // Add item to table
        table.setItems(data);
    }
}
