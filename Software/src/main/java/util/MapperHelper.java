package util;

import dto.MerchandiseDto;
import entity.Merchandise;

import java.util.ArrayList;
import java.util.List;

public class MapperHelper {
    public static List<MerchandiseDto> toMerchandiseModelList(List<Merchandise> merchandiseList) {
        List<MerchandiseDto> result = new ArrayList<>();

        for (Merchandise item : merchandiseList) {
            MerchandiseDto merchandiseDto = new MerchandiseDto();
            merchandiseDto.setId(item.getId());
            merchandiseDto.setName(item.getName());
            merchandiseDto.setType(item.getType());
            merchandiseDto.setBranch(item.getBranch());
            merchandiseDto.setQuantity(item.getQuantity());
            merchandiseDto.setImportPrice(item.getImportPrice());
            merchandiseDto.setPrice(item.getPrice().toString());
            merchandiseDto.setCreatedDate(item.getCreatedDate());
            merchandiseDto.setUpdatedDate(item.getUpdatedDate());

            result.add(merchandiseDto);
        }

        return result;
    }
}
