package repository;

import entity.Exports;
import org.hibernate.Session;
import org.hibernate.query.Query;
import util.HibernateUtils;

import java.util.List;

public class ExportsRepository {
    private static Session session;

    public static List<Exports> getAll() {
        try {
            session = HibernateUtils.getSessionFactory().openSession();
            session.beginTransaction();
            Query<Exports> query = session.createQuery("" +
                    "SELECT r " +
                    "FROM Exports r");
            List<Exports> result = query.getResultList();
            session.getTransaction().commit();
            session.close();
            return result;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            session.close();
            return null;
        }
    }

    public static List<Exports> getLikeCustomerName(String customerName) {
        try {
            session = HibernateUtils.getSessionFactory().openSession();
            session.beginTransaction();
            Query<Exports> query = session.createQuery("" +
                    "SELECT r " +
                    "FROM Exports r " +
                    "WHERE r.orders.customer.fullName LIKE :customerName");
            query.setParameter("customerName", "%" + customerName + "%");
            List<Exports> result = query.getResultList();
            session.getTransaction().commit();
            session.close();
            return result;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            session.close();
            return null;
        }
    }
}
